# Airflow + MySQL Workflow

Containerized environment to work with Apache Airflow workflows using MySQL database as the default metadata relational database instead of SQLite.

Observations:

- This project is a work in progress, some things like the composer are not ready yet.
- I automatically set the SequentialExecutor to LocalExecutor

## Getting started

### Create the images (inside Dockerfile folders):

docker build -t airflow . <br>
docker build -t airflow_db .

### MySQL container:
docker run --name airflow_mysql -e MYSQL_ROOT_PASSWORD=toor -p 3360:3306 -d airflow_db

### Airflow Container:
docker run -it -p 8080:8080 --link airflow_mysql:localhost -d airflow
